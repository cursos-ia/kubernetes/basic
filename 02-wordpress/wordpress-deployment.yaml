apiVersion: apps/v1
kind: Deployment
metadata:
  name: wordpress-deployment
  namespace: wordpress-ns
  labels:
    app.kubernetes.io/name: wordpress
    app.kubernetes.io/part-of: wordpress
    app.kubernetes.io/component: application
    app.kubernetes.io/instance: wordpress-instance
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: wordpress
  template:
    metadata:
      creationTimestamp: null
      labels:
        app.kubernetes.io/name: wordpress
        app.kubernetes.io/part-of: wordpress
        app.kubernetes.io/component: application
        app.kubernetes.io/instance: wordpress-instance
    spec:
      volumes:
        - name: empty-dir
          emptyDir: {}
        - name: wordpress-data
          persistentVolumeClaim:
            claimName: wordpress-pvc
      initContainers:
        - name: prepare-base-dir
          image: docker.io/bitnami/wordpress:6.5.2-debian-12-r4
          command:
            - /bin/bash
          args:
            - '-ec'
            - >
              #!/bin/bash


              . /opt/bitnami/scripts/liblog.sh

              . /opt/bitnami/scripts/libfs.sh


              info "Copying base dir to empty dir"

              # In order to not break the application functionality (such as
              upgrades or plugins) we need

              # to make the base directory writable, so we need to copy it to an
              empty dir volume

              cp -r --preserve=mode /opt/bitnami/wordpress
              /emptydir/app-base-dir


              info "Copying symlinks to stdout/stderr"

              # We copy the logs folder because it has symlinks to stdout and
              stderr

              if ! is_dir_empty /opt/bitnami/apache/logs; then
                cp -r /opt/bitnami/apache/logs /emptydir/apache-logs-dir
              fi


              info "Copying default PHP config"

              cp -r --preserve=mode /opt/bitnami/php/etc /emptydir/php-conf-dir


              info "Copy operation completed"
          resources:
            limits:
              cpu: 375m
              memory: 384Mi
            requests:
              cpu: 250m
              memory: 256Mi
          volumeMounts:
            - name: empty-dir
              mountPath: /emptydir
          securityContext:
            capabilities:
              drop:
                - ALL
            privileged: false
            runAsUser: 1001
            runAsGroup: 1001
            runAsNonRoot: true
            readOnlyRootFilesystem: true
      containers:
        - name: wordpress
          image: docker.io/bitnami/wordpress:6.5.2-debian-12-r4
          ports:
            - name: http
              containerPort: 8080
              protocol: TCP
            - name: https
              containerPort: 8443
              protocol: TCP
          env:
            - name: BITNAMI_DEBUG
              value: 'false'
            - name: ALLOW_EMPTY_PASSWORD
              value: 'yes'
            - name: WORDPRESS_SKIP_BOOTSTRAP
              value: 'no'
            - name: MARIADB_HOST
              value: mariadb-svc
            - name: MARIADB_PORT_NUMBER
              value: '3306'
            - name: WORDPRESS_DATABASE_NAME
              value: bitnami_wordpress
            - name: WORDPRESS_DATABASE_USER
              value: bn_wordpress
            - name: WORDPRESS_DATABASE_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: mariadb-secret
                  key: mariadb-password
            - name: WORDPRESS_USERNAME
              value: user
            - name: WORDPRESS_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: wordpress-secret
                  key: wordpress-password
            - name: WORDPRESS_EMAIL
              value: user@example.com
            - name: WORDPRESS_FIRST_NAME
              value: FirstName
            - name: WORDPRESS_LAST_NAME
              value: LastName
            - name: WORDPRESS_HTACCESS_OVERRIDE_NONE
              value: 'no'
            - name: WORDPRESS_ENABLE_HTACCESS_PERSISTENCE
              value: 'no'
            - name: WORDPRESS_BLOG_NAME
              value: User's Blog!
            - name: WORDPRESS_TABLE_PREFIX
              value: wp_
            - name: WORDPRESS_SCHEME
              value: http
            - name: WORDPRESS_EXTRA_WP_CONFIG_CONTENT
            - name: WORDPRESS_PLUGINS
              value: none
            - name: WORDPRESS_OVERRIDE_DATABASE_SETTINGS
              value: 'no'
            - name: APACHE_HTTP_PORT_NUMBER
              value: '8080'
            - name: APACHE_HTTPS_PORT_NUMBER
              value: '8443'
          resources:
            limits:
              cpu: 375m
              memory: 384Mi
            requests:
              cpu: 250m
              memory: 256Mi
          volumeMounts:
            - name: empty-dir
              mountPath: /opt/bitnami/apache/conf
              subPath: apache-conf-dir
            - name: empty-dir
              mountPath: /opt/bitnami/apache/logs
              subPath: apache-logs-dir
            - name: empty-dir
              mountPath: /opt/bitnami/apache/var/run
              subPath: apcahe-tmp-dir
            - name: empty-dir
              mountPath: /opt/bitnami/php/etc
              subPath: php-conf-dir
            - name: empty-dir
              mountPath: /opt/bitnami/php/tmp
              subPath: php-tmp-dir
            - name: empty-dir
              mountPath: /opt/bitnami/php/var
              subPath: php-var-dir
            - name: empty-dir
              mountPath: /tmp
              subPath: tmp-dir
            - name: empty-dir
              mountPath: /opt/bitnami/wordpress
              subPath: app-base-dir
            - name: wordpress-data
              mountPath: /bitnami/wordpress
              subPath: wordpress
          livenessProbe:
            httpGet:
              path: /wp-admin/install.php
              port: http
              scheme: HTTP
            initialDelaySeconds: 120
            timeoutSeconds: 5
            periodSeconds: 10
            successThreshold: 1
            failureThreshold: 6
          readinessProbe:
            httpGet:
              path: /wp-login.php
              port: http
              scheme: HTTP
            initialDelaySeconds: 30
            timeoutSeconds: 5
            periodSeconds: 10
            successThreshold: 1
            failureThreshold: 6
          securityContext:
            capabilities:
              drop:
                - ALL
            privileged: false
            runAsUser: 1001
            runAsGroup: 1001
            runAsNonRoot: true
            readOnlyRootFilesystem: true
      restartPolicy: Always
      serviceAccountName: wordpress-sa
      securityContext:
        fsGroup: 1001
        runAsUser: 1001
        fsGroupChangePolicy: Always
      hostAliases:
        - ip: 127.0.0.1
          hostnames:
            - status.localhost
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 25%
      maxSurge: 25%
  revisionHistoryLimit: 10
  progressDeadlineSeconds: 600
