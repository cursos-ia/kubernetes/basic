# Wordpress on Kubernetes

## Getting started
Step by Step how deploy Wordpress in Kubernetes Cluster

### Create namespace for Wordpress installation
kubectl apply -f namespace.yaml

### Create ConfigMap & Secrets for MariaDB
kubectl apply -f mariadb-secret.yaml
kubectl apply -f mariadb-cm.yaml

### Create Service Account for MariaDB
kubectl apply -f mariadb-sa.yaml

### Create Persistent Volume Claim for MariaDB
kubectl apply -f mariadb-pvc.yaml

### Create StatefulSets for MariaDB
kubectl apply -f mariadb-ss.yaml

### Create Service for MariaDB
kubectl apply -f mariadb-svc.yaml


### Create Secrets for Wordpress
kubectl apply -f wordpress-secret.yaml

### Create Service Account for Wordpress
kubectl apply -f wordpress-sa.yaml

### Create Persistent Volume Claim for Wordpress
kubectl apply -f wordpress-pvc.yaml

### Create Replica Sets for Wordpress
kubectl apply -f wordpress-deployment.yaml

### Create Service LoadBalancer for Wordpress
kubectl apply -f wordpress-svc.yaml