# Simple Service on Kubernetes

## Getting started
Step by Step how deploy Simple Service in Kubernetes Cluster

### Deploy Simple Service as POD
kubectl apply -f hello-world-pod.yaml
kubectl get pods -a
kubectl delete -f hello-world-pod.yaml

### Deploy Simple Service as Deployment
kubectl apply -f hello-world-deployment.yaml

### Expose Simple Service
kubectl apply -f hello-world-service.yaml

### Inspect Simple Service
kubectl get services hello-world-service

### Way to access this service from a web browser
kubectl port-forward service/hello-world-service 7080:8080

### Other way to access this service from a web browser
kubectl apply -f hello-world-loadbalancer.yaml
kubectl get services
minikube tunnel
kubectl get services

### Finally other way access this service is with ingress controller
minikube addons enable ingress
kubectl apply -f hello-world-ingress.yaml
kubectl get ingress