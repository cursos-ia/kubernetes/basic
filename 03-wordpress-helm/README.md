# Wordpress on Kubernetes using Helm

## Getting started
Step by Step how deploy Wordpress in Kubernetes Cluster using Helm Chart

### Download Helm repository for wordpress
https://artifacthub.io/packages/helm/bitnami/wordpress

helm repo add bitnami https://charts.bitnami.com/bitnami

### List Helm repositories
helm repo list

### Update Helm repositories
helm repo update

### Install Wordpress with Helm
helm install \
    --namespace wordpress-ns \
    --create-namespace \
    -f values.yaml \
    --version 22.2.7 \
    my-wordpress \
    bitnami/wordpress

### Check Wordpress
./show-loadbalancer-endpoint.sh

### Uninstall Wordpress with Helm
helm uninstall \
    --namespace wordpress-ns \
    my-wordpress
