# https://artifacthub.io/packages/helm/bitnami/wordpress

helm repo add bitnami https://charts.bitnami.com/bitnami

helm repo list

helm repo update

helm install \
    --namespace wordpress-ns \
    --create-namespace \
    -f values.yaml \
    --version 22.2.7 \
    my-wordpress \
    bitnami/wordpress
